#version 330 core

out vec4 color;
in vec3 pos_worldspace;
in vec3 frag_normal;

uniform vec3 light_pos;
uniform vec3 camera_pos;
uniform vec3 material_color;

void main()
{
    vec3 ambient = 0.1 * material_color;

    vec3 light_dir = normalize(light_pos - pos_worldspace);
    float diffuse_strength = max(dot(frag_normal, light_dir), 0.0);
    vec3 diffuse = diffuse_strength * material_color;

    vec3 camera_dir = normalize(camera_pos - pos_worldspace);

    // blinn phong specular
    vec3 halfway = normalize(light_dir + camera_dir);
    float specular_strength = pow(max(dot(frag_normal, halfway), 0.), 32.);

    vec3 specular_color = mix(vec3(1., 1., .9), material_color, .5);
    vec3 specular = specular_strength * specular_color;

    color = vec4(ambient + diffuse + specular, 1.0);
}
