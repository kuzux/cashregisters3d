#pragma once

#include <array>
#include <string>

#include "Error.h"
#include "GLTypes.h"

class GLCubemap
{
public:
    struct Binding {
        friend class GLCubemap;
        Binding(GLCubemap& cubemap);
        ~Binding();

    private:
        GLCubemap& m_cubemap;
        GL::uint m_old_glid { 0 };
    };

    GLCubemap() { }
    ~GLCubemap() { }

    // texture filename order: +x, -x, +y, -y, +z, -z
    ErrorOr<void> load(std::array<std::string, 6> const& filenames);
    bool is_loaded() const { return m_is_loaded; }

    [[nodiscard]] Binding bind();

private:
    GL::uint m_texture_glid { 0 };

    bool m_is_loaded { false };
    bool m_is_bound { false };
};
