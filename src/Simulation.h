#pragma once

#include <stdint.h>

#include <queue>
#include <vector>

#include "Emitter.h"

struct CashRegisterEvent {
    enum class Type {
        AddCustomerToQueue,
        RemoveCustomerFromQueue,
    };

    Type type;

    // Which queue are we adding to/removing from?
    uint32_t target;

    // What is the delay of the customer (in milliseconds)
    // i.e. How much does the customer take to be served by the cashier
    uint32_t delay;
};

class Simulation : public Emitter<CashRegisterEvent::Type, CashRegisterEvent>
{
public:
    Simulation(uint32_t number_of_queues);
    ~Simulation();

    void add_customer(uint32_t delay);
    void tick(uint64_t ticks);

private:
    uint64_t m_last_tick;

    // each queue element is how many ms does the customer have remaining to be served
    std::vector<std::queue<uint32_t>> m_queues;
};
