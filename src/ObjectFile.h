#pragma once

#include <string>
#include <string_view>
#include <vector>

#include <glm/glm.hpp>

#include "Error.h"
#include "Tokenizer.h"

class ObjectFile
{
public:
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 uv;
    };

    static ErrorOr<ObjectFile> load(std::string const& filename);

    // TODO: have an element buffer as well
    // TODO: handle materials
    std::vector<Vertex> const& vertices() const { return m_vertices; }
    std::string const& name() const { return m_name; }

private:
    ObjectFile() = default;
    ErrorOr<void> handle_line(std::string_view line);

    ErrorOr<int> parse_int(std::string_view str);
    ErrorOr<float> parse_float(std::string_view str);
    ErrorOr<glm::vec3> parse_vec3(Tokenizer& tok);
    ErrorOr<glm::vec2> parse_vec2(Tokenizer& tok);

    ErrorOr<Vertex> parse_face_token(std::string_view token);

    std::string m_name;

    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec2> m_uvs;

    std::vector<Vertex> m_vertices;
};
