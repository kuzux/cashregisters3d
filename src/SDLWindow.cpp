#include "SDLWindow.h"
#include "vendor/stb_image.h"
#include <fmt/core.h>

#include <atomic>

#ifdef USE_IMGUI
#define _STR(x) #x
#define STR(x) _STR(x)

#include STR(IMGUI_HEADER(imgui_impl_opengl3.h))
#include STR(IMGUI_HEADER(imgui_impl_sdl2.h))

#undef _STR
#undef STR

static std::atomic<size_t> s_imgui_init_count = 0;
#endif

using namespace std;

#define SDL_MUST(expr)                                                                             \
    do {                                                                                           \
        int rc = (expr);                                                                           \
        if (rc != 0) {                                                                             \
            fmt::print(stderr, "{} returned {}: {}\n", #expr, rc, SDL_GetError());                 \
            exit(1);                                                                               \
        }                                                                                          \
    } while (0)

SDLWindow::SDLWindow(string_view title, glm::ivec2 size)
    : m_window_size(size)
{
    SDL_MUST(SDL_Init(SDL_INIT_VIDEO));

    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE));

    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));

    string title_copy(title);
    window = SDL_CreateWindow(title_copy.c_str(), 0, 0, size.x, size.y,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    assert(window);

    ctx = SDL_GL_CreateContext(window);
    SDL_MUST(SDL_GL_MakeCurrent(window, ctx));

    SDL_MUST(SDL_GL_SetSwapInterval(1));
}

SDLWindow::~SDLWindow()
{
#ifdef USE_IMGUI
    if (m_using_imgui) {
        if (--s_imgui_init_count == 0) {
            ImGui_ImplOpenGL3_Shutdown();
            ImGui_ImplSDL2_Shutdown();
            ImGui::DestroyContext();
        }
    }
#endif
    if (m_window_icon)
        SDL_FreeSurface(m_window_icon);
    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDLWindow::draw()
{
    assert(window);

    SDL_GL_SwapWindow(window);
    m_vsynced = true;
}

void SDLWindow::set_title(string_view title)
{
    assert(window);

    string title_copy(title);
    SDL_SetWindowTitle(window, title_copy.c_str());
}

void SDLEventLoop::run(function<void(uint64_t, uint64_t)> body)
{
    m_running = true;
    int frame = 0;
    uint64_t prev_ticks = SDL_GetTicks64();
    while (m_running) {
        uint64_t ticks = SDL_GetTicks64();
        uint64_t delta_ticks = ticks - prev_ticks;

        // TODO: Report these performance statistics in a nicer way (optionally, obviously)
        // fmt::print("frame {}\n", frame++);
        // fmt::print("ms/frame {}\n", (float)ticks / (float)frame);
        // fmt::print("ms since last frame {}\n", ticks - prev_ticks);
        prev_ticks = ticks;
        m_window.m_vsynced = false;

        while (!m_timeouts.empty() && m_timeouts.front().should_call(ticks)) {
            auto timeout = m_timeouts.front();
            m_timeouts.pop();
            if (timeout.call(ticks) == Timeout::Result::Continue)
                m_timeouts.push(timeout);
        }

        SDL_Event evt;
        while (SDL_PollEvent(&evt)) {
#ifdef USE_IMGUI
            if (m_window.m_using_imgui)
                ImGui_ImplSDL2_ProcessEvent(&evt);
#endif

            if (evt.type == SDL_QUIT) {
                fmt::print("quitting\n");
                m_running = false;
                break;
            }

            emit((SDL_EventType)evt.type, evt);
        }
        if (!m_running)
            break;

        body(ticks, delta_ticks);
        // If we haven't drawn anything since we didn't need to, this means we haven't waited until
        // the V-Sync Which means we have to do the waiting ourselves (to limit our cpu usage). To
        // do that, anything close to but below 60fps is OK, so we just wait 15ms
        if (!m_window.m_vsynced)
            SDL_Delay(15);
    }
}

void SDLEventLoop::stop() { m_running = false; }

void SDLWindow::set_icon(string const& filename)
{
    assert(!m_window_icon);

    int w, h, c;
    void* data = stbi_load(filename.c_str(), &w, &h, &c, 4);
    assert(c == 4);

    m_window_icon = SDL_CreateRGBSurfaceWithFormat(0, w, h, c * 8, SDL_PIXELFORMAT_ARGB8888);
    assert(m_window_icon);

    SDL_LockSurface(m_window_icon);
    memcpy(m_window_icon->pixels, data, w * h * 4);
    SDL_UnlockSurface(m_window_icon);

    SDL_SetWindowIcon(window, m_window_icon);
    stbi_image_free(data);
}

void SDLEventLoop::run(function<void(uint64_t)> body)
{
    run([&](uint64_t ticks, uint64_t) { body(ticks); });
}

ErrorOr<void> SDLWindow::setup_imgui()
{
#ifndef USE_IMGUI
    return Error::with_message("imgui support not compiled in");
#else
    m_using_imgui = true;
    if (s_imgui_init_count++ > 0)
        return {};

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;

    ImGui::StyleColorsDark();

    ImGui_ImplSDL2_InitForOpenGL(window, ctx);
    ImGui_ImplOpenGL3_Init("#version 330");

    return {};
#endif
}

SDLEventLoop& SDLWindow::event_loop()
{
    if (!m_event_loop)
        m_event_loop = new SDLEventLoop(*this);
    assert(m_event_loop);
    return *m_event_loop;
}

SDLEventLoop::SDLEventLoop(SDLWindow& window)
    : m_window(window)
{
}

Timeout::Timeout(uint64_t interval, Callback callback)
    : m_interval(interval)
    , m_callback(callback)
{
    m_last_call = 0;
}

Timeout::~Timeout() { }

bool Timeout::should_call(uint64_t ticks) const { return ticks - m_last_call >= m_interval; }

Timeout::Result Timeout::call(uint64_t ticks)
{
    m_last_call = ticks;
    return m_callback(ticks);
}

void SDLEventLoop::in(uint64_t interval, Timeout::Callback callback)
{
    m_timeouts.push(Timeout(interval, callback));
}
