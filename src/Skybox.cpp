#include "Skybox.h"

#include "GLArrayBuffer.h"
#include "GLFramework.h"
#include "GLShader.h"
#include "GLVertexArray.h"

#include <array>
#include <fstream>
#include <sstream>

using namespace std;

struct VertexInfo {
    glm::vec3 position;
};

struct Skybox::Impl {
    GLArrayBuffer<VertexInfo> vertex_buffer { GLBufferTarget::ArrayBuffer };
    GLVertexArray vertex_array;
    GLProgram program;
    GLCubemap* cubemap { nullptr };

    GLArrayBuffer<VertexInfo>::Binding* vertex_buffer_binding { nullptr };
    GLVertexArray::Binding* vertex_array_binding { nullptr };
    GLProgram::Binding* program_binding { nullptr };
    GLCubemap::Binding* cubemap_binding { nullptr };
};

// clang-format off
array<VertexInfo, 36> vertex_buffer_data = {{
    // positions          
    {{ -1.0f,  1.0f, -1.0f }},
    {{ -1.0f, -1.0f, -1.0f }},
    {{  1.0f, -1.0f, -1.0f }},
    {{  1.0f, -1.0f, -1.0f }},
    {{  1.0f,  1.0f, -1.0f }},
    {{ -1.0f,  1.0f, -1.0f }},

    {{ -1.0f, -1.0f,  1.0f }},
    {{ -1.0f, -1.0f, -1.0f }},
    {{ -1.0f,  1.0f, -1.0f }},
    {{ -1.0f,  1.0f, -1.0f }},
    {{ -1.0f,  1.0f,  1.0f }},
    {{ -1.0f, -1.0f,  1.0f }},

    {{  1.0f, -1.0f, -1.0f }},
    {{  1.0f, -1.0f,  1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{  1.0f,  1.0f, -1.0f }},
    {{  1.0f, -1.0f, -1.0f }},

    {{ -1.0f, -1.0f,  1.0f }},
    {{ -1.0f,  1.0f,  1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{  1.0f, -1.0f,  1.0f }},
    {{ -1.0f, -1.0f,  1.0f }},

    {{ -1.0f,  1.0f, -1.0f }},
    {{  1.0f,  1.0f, -1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{  1.0f,  1.0f,  1.0f }},
    {{ -1.0f,  1.0f,  1.0f }},
    {{ -1.0f,  1.0f, -1.0f }},

    {{ -1.0f, -1.0f, -1.0f }},
    {{ -1.0f, -1.0f,  1.0f }},
    {{  1.0f, -1.0f, -1.0f }},
    {{  1.0f, -1.0f, -1.0f }},
    {{ -1.0f, -1.0f,  1.0f }},
    {{  1.0f, -1.0f,  1.0f }}
}};
// clang-format on

Skybox::Skybox()
    : m_impl(new Impl)
{
}

Skybox::~Skybox() { delete m_impl; }

ErrorOr<void> Skybox::load(GLCubemap& cubemap, array<string, 2> shader_filenames)
{
    {
        auto binding = m_impl->vertex_buffer.bind();
        m_impl->vertex_buffer.load(vertex_buffer_data);

        auto array_binding = m_impl->vertex_array.bind();
        m_impl->vertex_array.define_attribute(
            0, GLVertexAttribute(glm::vec3, VertexInfo, position));
    }

    m_impl->cubemap = &cubemap;

    auto skybox_vertex_shader
        = MUST(GLShader::from_file(shader_filenames[0], GLShader::Type::Vertex));
    auto skybox_fragment_shader
        = MUST(GLShader::from_file(shader_filenames[1], GLShader::Type::Fragment));

    m_impl->program.attach(skybox_vertex_shader);
    m_impl->program.attach(skybox_fragment_shader);

    MUST(m_impl->program.link());

    m_is_loaded = true;

    return {};
}

Skybox::Binding::Binding(Skybox& cubemap)
    : m_skybox(cubemap)
{
    assert(m_skybox.m_is_loaded);
    assert(m_skybox.m_impl->cubemap->is_loaded());
    assert(!m_skybox.m_is_bound);

    assert(!m_skybox.m_impl->vertex_buffer_binding);
    assert(!m_skybox.m_impl->vertex_array_binding);
    assert(!m_skybox.m_impl->program_binding);
    assert(!m_skybox.m_impl->cubemap_binding);

    m_skybox.m_impl->vertex_buffer_binding
        = new GLArrayBuffer<VertexInfo>::Binding(m_skybox.m_impl->vertex_buffer);
    m_skybox.m_impl->vertex_array_binding
        = new GLVertexArray::Binding(m_skybox.m_impl->vertex_array);
    m_skybox.m_impl->program_binding = new GLProgram::Binding(m_skybox.m_impl->program);
    m_skybox.m_impl->cubemap_binding = new GLCubemap::Binding(*m_skybox.m_impl->cubemap);

    m_skybox.m_is_bound = true;
}

Skybox::Binding::~Binding()
{
    assert(m_skybox.m_impl->vertex_buffer_binding);
    assert(m_skybox.m_impl->vertex_array_binding);
    assert(m_skybox.m_impl->program_binding);
    assert(m_skybox.m_impl->cubemap_binding);

    delete m_skybox.m_impl->vertex_buffer_binding;
    delete m_skybox.m_impl->vertex_array_binding;
    delete m_skybox.m_impl->program_binding;
    delete m_skybox.m_impl->cubemap_binding;

    m_skybox.m_impl->vertex_buffer_binding = nullptr;
    m_skybox.m_impl->vertex_array_binding = nullptr;
    m_skybox.m_impl->program_binding = nullptr;
    m_skybox.m_impl->cubemap_binding = nullptr;

    m_skybox.m_is_bound = false;
}

Skybox::Binding Skybox::bind() { return Binding(*this); }

void Skybox::draw()
{
    assert(m_is_bound);

    glDepthMask(false);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glDepthMask(true);
}

void Skybox::set_projection_matrix(glm::mat4 const& projection_matrix)
{
    assert(m_is_bound);
    m_impl->program.get_uniform<glm::mat4>("projection").set_value(projection_matrix);
}

void Skybox::set_camera_matrix(glm::mat4 const& camera_matrix)
{
    assert(m_is_bound);
    m_impl->program.get_uniform<glm::mat4>("camera").set_value(camera_matrix);
}

GLCubemap const& Skybox::cubemap() const
{
    assert(m_is_loaded);
    return *m_impl->cubemap;
}
