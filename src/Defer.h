#pragma once
#include <optional>

template <typename Fn> class Defer
{
public:
    Defer(Fn fn)
        : m_fn(fn)
    {
    }
    ~Defer()
    {
        if (m_fn)
            (*m_fn)();
    }
    Defer(const Defer&) = delete;
    Defer(Defer&& o)
        : m_fn(o.m_fn)
    {
        o.m_fn = {};
    }

private:
    std::optional<Fn> m_fn;
};
