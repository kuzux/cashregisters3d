#pragma once

#include <optional>
#include <string_view>

class Tokenizer
{
public:
    Tokenizer(std::string_view str)
        : m_str(str)
    {
    }

    std::string_view str() const { return m_str; }
    void delimiter(char c) { m_delimiter = c; }
    std::optional<std::string_view> next_token();

private:
    bool is_delimiter(char c) const;

    std::optional<char> m_delimiter { std::nullopt };
    std::string_view m_str;
    size_t m_cursor { 0 };
};