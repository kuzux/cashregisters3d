#include "Tokenizer.h"
#include <ctype.h>
#include <fmt/core.h>
using namespace std;

optional<string_view> Tokenizer::next_token()
{
    if (m_cursor >= m_str.size())
        return {};

    bool found_token = false;
    size_t start = m_cursor;
    while (m_cursor <= m_str.size()) {
        if (!is_delimiter(m_str[m_cursor])) {
            found_token = true;
            m_cursor++;
            continue;
        }

        m_cursor++;
        break;
    }
    auto len = m_cursor - start - 1;
    if (len == 0)
        return {};
    if (!found_token)
        return {};

    return m_str.substr(start, len);
}

bool Tokenizer::is_delimiter(char c) const
{
    if (m_delimiter.has_value())
        return c == m_delimiter.value();
    return isspace(c);
}