#pragma once

#include <stdint.h>

namespace Random
{
void seed(uint32_t seed);

int uniform_int();
int uniform_int(int min, int max);

float uniform_float();
float uniform_float(float min, float max);

float normal_float();
float normal_float(float mean, float stddev);

// TODO: Add other distributions as well
}
