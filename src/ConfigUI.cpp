#include "ConfigUI.h"

#include "vendor/imgui.h"
#include "vendor/imgui_impl_opengl3.h"
#include "vendor/imgui_impl_sdl2.h"

#include <glm/gtc/matrix_transform.hpp>

void ConfigUI::draw()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("Config");

    ImGui::SliderFloat3("Camera position", &m_camera_pos.x, -10.0f, 10.0f);

    ImGui::SliderFloat3("Camera lookat", &m_camera_lookat.x, -10.0f, 10.0f);

    ImGui::SliderFloat("Mean service time", &m_customer_mean_time, 2.0f, 10.0f);
    ImGui::SliderFloat("Stddev of service time", &m_customer_stddev_time, 0.0f, 10.0f);

    if (ImGui::Button("Add customer")) {
        if (m_add_customer_cb)
            m_add_customer_cb(m_customer_mean_time, m_customer_stddev_time);
    }

    ImGui::End();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

glm::mat4 ConfigUI::view_matrix() const
{
    return glm::lookAt(m_camera_pos, m_camera_lookat, glm::vec3(0.0f, 0.0f, 1.0f));
}
