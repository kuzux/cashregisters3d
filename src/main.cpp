#include "AnimationController.h"
#include "Colorspace.h"
#include "ConfigUI.h"
#include "GLArrayBuffer.h"
#include "GLFramework.h"
#include "GLShader.h"
#include "GLVertexArray.h"
#include "ObjectFile.h"
#include "Random.h"
#include "SDLWindow.h"
#include "Simulation.h"

#include <algorithm>
#include <glm/gtc/matrix_transform.hpp>
#include <optional>
#include <string_view>
#include <vector>
using namespace std;

class Object
{
public:
    struct Binding {
        friend class Object;
        Binding(Object& object);
        ~Binding();

    private:
        Object& m_object;
    };
    Object(GLArrayBuffer<ObjectFile::Vertex>& vertex_buffer, GLVertexArray& vertex_array,
        GLProgram& program)
        : m_vertex_buffer(vertex_buffer)
        , m_vertex_array(vertex_array)
        , m_program(program)
    {
        m_num_vertices = m_vertex_buffer.size();
    }
    Object(Object&&) = default;
    ~Object() { }

    [[nodiscard]] Binding bind();
    void draw();

    glm::mat4 model_mat() const { return glm::translate(m_transform, m_position); }

    void transform(glm::mat4 transform) { m_transform = transform; }
    glm::vec3 position() const { return m_position; }
    void position(glm::vec3 position) { m_position = position; }

    int32_t lane_id() const { return m_lane_id; }
    void lane_id(uint32_t lane_id) { m_lane_id = lane_id; }

    bool deleted() const { return m_deleted; }
    void mark_as_deleted() { m_deleted = true; }

    glm::vec3 color() const { return m_color; }
    void color(glm::vec3 color) { m_color = color; }

    void animate_to(glm::vec3 new_pos, float duration);
    void update_animation(float delta);

private:
    // Model matrix = translate(position) * transform
    glm::mat4 m_transform { 1.0f };
    glm::vec3 m_position { 0.0f, 0.0f, 0.0f };

    glm::vec3 m_color { 1.0f, 1.0f, 1.0f };
    size_t m_num_vertices { 0 };

    GLArrayBuffer<ObjectFile::Vertex>& m_vertex_buffer;
    GLVertexArray& m_vertex_array;
    GLProgram& m_program;

    GLArrayBuffer<ObjectFile::Vertex>::Binding* m_vertex_buffer_binding { nullptr };
    GLVertexArray::Binding* m_vertex_array_binding { nullptr };
    GLProgram::Binding* m_program_binding { nullptr };

    optional<AnimationController<glm::vec3>> m_animation { nullopt };

    int32_t m_lane_id { -1 };
    bool m_deleted { false };
};

Object::Binding::Binding(Object& object)
    : m_object(object)
{
    assert(!m_object.m_vertex_buffer_binding);
    assert(!m_object.m_vertex_array_binding);
    assert(!m_object.m_program_binding);

    m_object.m_vertex_buffer_binding
        = new GLArrayBuffer<ObjectFile::Vertex>::Binding(m_object.m_vertex_buffer);
    m_object.m_vertex_array_binding = new GLVertexArray::Binding(m_object.m_vertex_array);
    m_object.m_program_binding = new GLProgram::Binding(m_object.m_program);
}

Object::Binding::~Binding()
{
    assert(m_object.m_vertex_buffer_binding);
    assert(m_object.m_vertex_array_binding);
    assert(m_object.m_program_binding);

    delete m_object.m_vertex_buffer_binding;
    delete m_object.m_vertex_array_binding;
    delete m_object.m_program_binding;

    m_object.m_vertex_buffer_binding = nullptr;
    m_object.m_vertex_array_binding = nullptr;
    m_object.m_program_binding = nullptr;
}

Object::Binding Object::bind() { return Binding(*this); }

void Object::draw()
{
    assert(m_vertex_buffer_binding);
    assert(m_vertex_array_binding);
    assert(m_program_binding);

    m_program.get_uniform<glm::mat4>("model").set_value(model_mat());

    glDrawArrays(GL_TRIANGLES, 0, m_vertex_buffer.size());
}

void Object::animate_to(glm::vec3 new_pos, float duration)
{
    m_animation = AnimationController(m_position, new_pos, duration);
}

void Object::update_animation(float delta)
{
    if (!m_animation)
        return;
    m_animation->update(delta);
    m_position = m_animation->value();
    if (m_animation->done())
        m_animation = nullopt;
}

struct CashRegistersState {
    GLArrayBuffer<ObjectFile::Vertex> cube_vertex_buffer { GLBufferTarget::ArrayBuffer };
    GLVertexArray cube_vertex_array;

    GLArrayBuffer<ObjectFile::Vertex> sphere_vertex_buffer { GLBufferTarget::ArrayBuffer };
    GLVertexArray sphere_vertex_array;

    GLProgram object_program;

    vector<Object> objects;
    vector<float> ends_of_lanes;

    ErrorOr<void> setup(glm::ivec2 window_size);
    void generate_cash_registers(int number_of_lanes);
    Object& add_customer_to_queue(int lane);
    void remove_customer_from_queue(int lane);
};

ErrorOr<void> CashRegistersState::setup(glm::ivec2 window_size)
{
    auto cube_obj_file = TRY(ObjectFile::load("assets/cube.obj"));

    {
        auto binding = cube_vertex_buffer.bind();
        cube_vertex_buffer.load(cube_obj_file.vertices());

        auto array_binding = cube_vertex_array.bind();
        cube_vertex_array.define_attribute(
            0, GLVertexAttribute(glm::vec3, ObjectFile::Vertex, position));
        cube_vertex_array.define_attribute(
            1, GLVertexAttribute(glm::vec3, ObjectFile::Vertex, normal));
        cube_vertex_array.define_attribute(2, GLVertexAttribute(glm::vec2, ObjectFile::Vertex, uv));
    }

    auto sphere_obj_file = TRY(ObjectFile::load("assets/sphere.obj"));
    {
        auto binding = sphere_vertex_buffer.bind();
        sphere_vertex_buffer.load(sphere_obj_file.vertices());

        auto array_binding = sphere_vertex_array.bind();
        sphere_vertex_array.define_attribute(
            0, GLVertexAttribute(glm::vec3, ObjectFile::Vertex, position));
        sphere_vertex_array.define_attribute(
            1, GLVertexAttribute(glm::vec3, ObjectFile::Vertex, normal));
        sphere_vertex_array.define_attribute(
            2, GLVertexAttribute(glm::vec2, ObjectFile::Vertex, uv));
    }

    auto vertex_shader
        = TRY(GLShader::from_file("assets/object.vert.glsl", GLShader::Type::Vertex));
    auto fragment_shader
        = TRY(GLShader::from_file("assets/object.frag.glsl", GLShader::Type::Fragment));

    object_program.attach(vertex_shader);
    object_program.attach(fragment_shader);
    TRY(object_program.link());

    auto aspect_ratio = (float)window_size.x / (float)window_size.y;
    auto projection_mat = glm::perspective(glm::radians(45.0f), aspect_ratio, 0.1f, 100.0f);

    auto light_pos = glm::vec3(3.0f, 4.0f, 5.0f);

    {
        auto binding = object_program.bind();

        object_program.get_uniform<glm::mat4>("projection").set_value(projection_mat);
        object_program.get_uniform<glm::vec3>("light_pos").set_value(light_pos);
    }

    return {};
}

static float object_scale = 0.2f;
static float distance_between_lanes = 3.f;

void CashRegistersState::generate_cash_registers(int number_of_lanes)
{
    glm::vec3 register_pos { 0.0f, 0.0f, 0.0f };
    for (int i = 0; i < number_of_lanes; ++i) {
        register_pos.x = i * distance_between_lanes;
        objects.emplace_back(sphere_vertex_buffer, sphere_vertex_array, object_program);

        glm::mat4 transform { 1.0f };
        transform = glm::scale(transform, glm::vec3(object_scale));
        objects.back().transform(transform);
        objects.back().position(register_pos);
        objects.back().color(glm::vec3(0.8f, 0.2f, 0.2f));

        ends_of_lanes.push_back(register_pos.y + distance_between_lanes);
    }
}

Object& CashRegistersState::add_customer_to_queue(int lane)
{
    objects.emplace_back(cube_vertex_buffer, cube_vertex_array, object_program);
    auto& obj = objects.back();
    obj.lane_id(lane);

    glm::mat4 transform { 1.0f };
    transform = glm::scale(transform, glm::vec3(object_scale));
    obj.transform(transform);
    obj.position(glm::vec3(3 * distance_between_lanes, 25.f, 0.0f));
    obj.animate_to(glm::vec3(lane * distance_between_lanes, ends_of_lanes[lane], 0.0f), 2.0f);

    auto color_oklab = glm::vec3(0.6f);
    color_oklab.y = Random::uniform_float(-0.4f, 0.4f);
    color_oklab.z = Random::uniform_float(-0.4f, 0.4f);
    auto color_rgb = Colorspace::oklab_to_linear_srgb(color_oklab);
    obj.color(color_rgb);

    ends_of_lanes[lane] += distance_between_lanes;
    return obj;
}

void CashRegistersState::remove_customer_from_queue(int lane)
{
    auto it = find_if(objects.begin(), objects.end(),
        [&](auto& obj) { return !obj.deleted() && obj.lane_id() == lane; });
    if (it != objects.end()) {
        ends_of_lanes[lane] -= distance_between_lanes;
        it->mark_as_deleted();
    }

    float target_x_coordinate = lane * distance_between_lanes;
    float next_y_coordinate = distance_between_lanes;

    for (auto& obj : objects) {
        if (obj.deleted())
            continue;

        if (obj.lane_id() == lane) {
            auto pos = glm::vec3(target_x_coordinate, next_y_coordinate, 0.0f);
            next_y_coordinate += distance_between_lanes;
            obj.animate_to(pos, 1.0f);
        }
    }
}

int main()
{
    Random::seed(0);

    SDLWindow window("Cash register simulation", { 1024, 720 });
    MUST(window.setup_imgui());

    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.5f, 0.1f, 1.0f);

    CashRegistersState state;
    MUST(state.setup(window.window_size()));
    state.generate_cash_registers(5);

    auto& event_loop = window.event_loop();
    Simulation sim(5);
    sim.on(CashRegisterEvent::Type::AddCustomerToQueue, [&](CashRegisterEvent const& event) {
        fmt::print("Add customer to queue {}\n", event.target);
        state.add_customer_to_queue(event.target);
    });
    sim.on(CashRegisterEvent::Type::RemoveCustomerFromQueue, [&](CashRegisterEvent const& event) {
        fmt::print("Remove customer from queue {}\n", event.target);
        state.remove_customer_from_queue(event.target);
    });

    ConfigUI config;
    config.on_add_customer([&](float mean, float stddev) {
        auto delay_in_secs = Random::normal_float(mean, stddev);
        if (delay_in_secs < 0)
            delay_in_secs = 0;
        uint64_t delay_in_ms = delay_in_secs * 1000;
        sim.add_customer(delay_in_ms);
    });
    event_loop.run([&](uint64_t ticks, uint64_t delta_ticks) {
        float delta_secs = delta_ticks / 1000.0f;
        sim.tick(ticks);
        for (auto& object : state.objects) {
            if (object.deleted())
                continue;
            object.update_animation(delta_secs);
        }
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        {
            auto binding = state.object_program.bind();
            auto view_mat = config.view_matrix();
            auto camera_pos = config.camera_pos();

            state.object_program.get_uniform<glm::vec3>("camera_pos").set_value(camera_pos);
            state.object_program.get_uniform<glm::mat4>("view").set_value(view_mat);
        }

        for (auto& object : state.objects) {
            if (object.deleted())
                continue;
            auto binding = object.bind();
            state.object_program.get_uniform<glm::vec3>("material_color").set_value(object.color());
            object.draw();
        }

        config.draw();
        window.draw();
    });

    return 0;
}
