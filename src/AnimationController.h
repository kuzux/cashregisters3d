#pragma once

template <typename T> class AnimationController
{
public:
    AnimationController(T start, T target, float duration)
        : m_start(start)
        , m_target(target)
        , m_diff(target - start)
        , m_value(start)
        , m_duration(duration)
    {
    }

    void update(float dt)
    {
        m_elapsed += dt;
        if (m_elapsed > m_duration)
            m_elapsed = m_duration;

        // TODO: Add easing functions
        float t = m_elapsed / m_duration;
        m_value = m_start + m_diff * t;
    }

    bool done() const { return m_elapsed >= m_duration; }
    T value() const { return m_value; }

private:
    T m_start;
    T m_target;

    T m_diff;
    T m_value;

    float m_duration;
    float m_elapsed { 0.0f };
};
