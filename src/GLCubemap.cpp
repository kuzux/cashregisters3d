#include "GLCubemap.h"

#include "GLFramework.h"
#include "vendor/stb_image.h"

using namespace std;

ErrorOr<void> GLCubemap::load(array<string, 6> const& filenames)
{
    glGenTextures(1, &m_texture_glid);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture_glid);

    for (size_t i = 0; i < filenames.size(); ++i) {
        auto filename = filenames[i];
        int w, h, c;
        void* data = stbi_load(filename.c_str(), &w, &h, &c, 3);
        if (!data)
            return Error::with_message("Failed to load image " + filename);
        if (c < 3)
            return Error::with_message("Image " + filename + " has less than 3 channels");

        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        stbi_image_free(data);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    m_is_loaded = true;
    return {};
}

GLCubemap::Binding::Binding(GLCubemap& cubemap)
    : m_cubemap(cubemap)
{
    assert(!m_cubemap.m_is_bound);
    assert(m_cubemap.m_is_loaded);

    GL::int_ currently_bound_cubemap;
    glGetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP, &currently_bound_cubemap);
    assert(currently_bound_cubemap >= 0);
    m_old_glid = currently_bound_cubemap;

    if (m_cubemap.m_texture_glid != m_old_glid)
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubemap.m_texture_glid);
    m_cubemap.m_is_bound = true;
}

GLCubemap::Binding::~Binding()
{
    assert(m_cubemap.m_is_bound);
    GL::int_ currently_bound_cubemap;
    glGetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP, &currently_bound_cubemap);
    assert(currently_bound_cubemap == (GL::int_)m_cubemap.m_texture_glid);

    if (m_cubemap.m_texture_glid != m_old_glid)
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_old_glid);
    m_cubemap.m_is_bound = false;
}

GLCubemap::Binding GLCubemap::bind() { return Binding(*this); }
