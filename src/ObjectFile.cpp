#include "ObjectFile.h"

#include <charconv>
#include <fmt/core.h>
#include <fstream>

using namespace std;

ErrorOr<int> ObjectFile::parse_int(string_view str)
{
    int value;
    auto res = from_chars(str.data(), str.data() + str.size(), value);
    if (res.ec == errc::invalid_argument || res.ec == errc::result_out_of_range)
        return Error::with_message(fmt::format("Could not parse int {}", str));
    return value;
}

ErrorOr<float> ObjectFile::parse_float(string_view str)
{
    try {
        return stof(string(str));
    } catch (...) {
        return Error::with_message(fmt::format("Could not parse float {}", str));
    }
}

ErrorOr<glm::vec2> ObjectFile::parse_vec2(Tokenizer& tok)
{
    glm::vec2 res;

    auto n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse vec2 {}", tok.str()));
    res.x = TRY(parse_float(*n));

    n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse vec2 {}", tok.str()));
    res.y = TRY(parse_float(*n));

    return res;
}

ErrorOr<glm::vec3> ObjectFile::parse_vec3(Tokenizer& tok)
{
    glm::vec3 res;

    auto n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse vec3 {}", tok.str()));
    res.x = TRY(parse_float(*n));

    n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse vec3 {}", tok.str()));
    res.y = TRY(parse_float(*n));

    n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse vec3 {}", tok.str()));
    res.z = TRY(parse_float(*n));

    return res;
}

ErrorOr<ObjectFile> ObjectFile::load(string const& filename)
{
    ifstream file(filename);
    if (!file)
        return Error::with_message(fmt::format("Could not open file {}", filename));

    ObjectFile res;

    string line;
    while (getline(file, line)) {
        TRY(res.handle_line(line));
    }

    return res;
}

ErrorOr<ObjectFile::Vertex> ObjectFile::parse_face_token(string_view token)
{
    // TODO: This can be in v/vt format in addition to the v/vt/vn format
    Vertex res;
    Tokenizer tok(token);
    tok.delimiter('/');

    auto n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse face token {}", token));
    res.position = m_positions[TRY(parse_int(*n)) - 1];

    n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse face token {}", token));
    res.uv = m_uvs[TRY(parse_int(*n)) - 1];

    n = tok.next_token();
    if (!n)
        return Error::with_message(fmt::format("Could not parse face token {}", token));
    res.normal = m_normals[TRY(parse_int(*n)) - 1];

    return res;
}

ErrorOr<void> ObjectFile::handle_line(string_view line)
{
    if (line.empty())
        return {};

    // fmt::print("Handling line {}\n", line);

    Tokenizer tok(line);
    auto token = tok.next_token();
    if (!token)
        return Error::with_message(fmt::format("Could not parse line {}", line));

    if (*token == "#")
        return {};

    if (*token == "mtllib" || *token == "usemtl") {
        fmt::print("TODO: handle mtllib and usemtl\n");
        return {};
    }

    if (*token == "s") {
        fmt::print("TODO: handle s\n");
        return {};
    }

    if (*token == "o") {
        token = tok.next_token();
        m_name = *token;
        return {};
    }

    if (*token == "v") {
        auto pos = TRY(parse_vec3(tok));
        m_positions.push_back(pos);
        return {};
    }

    if (*token == "vn") {
        auto normal = TRY(parse_vec3(tok));
        m_normals.push_back(normal);
        return {};
    }

    if (*token == "vt") {
        auto uv = TRY(parse_vec2(tok));
        m_uvs.push_back(uv);
        return {};
    }

    if (*token == "f") {
        // TODO: Faces can have an arbitrary number of vertices
        token = tok.next_token();
        if (!token)
            return Error::with_message(fmt::format("Could not parse line {}", line));
        Vertex vert = TRY(parse_face_token(*token));
        m_vertices.push_back(vert);

        token = tok.next_token();
        if (!token)
            return Error::with_message(fmt::format("Could not parse line {}", line));
        vert = TRY(parse_face_token(*token));
        m_vertices.push_back(vert);

        token = tok.next_token();
        if (!token)
            return Error::with_message(fmt::format("Could not parse line {}", line));
        vert = TRY(parse_face_token(*token));
        m_vertices.push_back(vert);
        return {};
    }

    return Error::with_message(fmt::format("Could not parse line {}", line));
}