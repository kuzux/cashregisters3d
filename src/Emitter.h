#pragma once

#include <functional>
#include <unordered_map>

template <typename EventType, typename Event> class Emitter
{
public:
    using Callback = std::function<void(Event)>;

    void on(EventType type, Callback cb)
    {
        assert(m_callbacks.find(type) == m_callbacks.end());
        m_callbacks.insert({ type, cb });
    }

protected:
    void emit(EventType type, Event evt)
    {
        auto it = m_callbacks.find(type);
        if (it != m_callbacks.end())
            it->second(evt);
    }

    std::unordered_map<EventType, Callback> m_callbacks;
};
