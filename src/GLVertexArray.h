#pragma once

#include <cassert>
#include <functional>
#include <unordered_set>

#include "GLTypes.h"

class GLVertexArray
{
public:
    struct Binding {
        friend class GLVertexArray;
        Binding(GLVertexArray& vao);
        ~Binding();

    private:
        GLVertexArray& m_vao;
        GL::uint m_old_glid { 0 };
    };

    struct Attribute {
        int32_t num_components;
        GL::enum_ gl_type;
        size_t stride;
        size_t offset;

        size_t divisor;
    };

    GLVertexArray();
    GLVertexArray(GLVertexArray&& o);
    ~GLVertexArray();

    [[nodiscard]] Binding bind();

    // TODO: This should be fallible
    void define_attribute(uint32_t index, Attribute attr);

private:
    GL::uint m_glid { 0 };
    bool m_is_bound { false };
    std::unordered_set<uint32_t> m_enabled_attrs;
};

// Is there a better way to do a compile time switch case based on type?
int32_t __num_components_gl_attribute(const char* class_name);
GL::enum_ __gl_type_gl_attribute(const char* class_name);

// TODO: Support integral & matrix types as well
#define __GLVertexAttribute_Impl(type_name, struct_name, field_name, divisor)                      \
    GLVertexArray::Attribute                                                                       \
    {                                                                                              \
        __num_components_gl_attribute(#type_name), __gl_type_gl_attribute(#type_name),             \
            sizeof(struct_name), offsetof(struct_name, field_name), divisor                        \
    }
#define GLVertexAttribute(type_name, struct_name, field_name)                                      \
    __GLVertexAttribute_Impl(type_name, struct_name, field_name, 0)
#define GLInstancedAttribute(type_name, struct_name, field_name)                                   \
    __GLVertexAttribute_Impl(type_name, struct_name, field_name, 1)
