#include "Random.h"

#include <math.h>
#include <stdlib.h>

namespace Random
{
// TODO: Use mt19937 instead of rand
void seed(uint32_t seed) { srand(seed); }

int uniform_int() { return rand(); }
int uniform_int(int min, int max) { return min + uniform_int() % (max - min); }

float uniform_float() { return static_cast<float>(uniform_int()) / RAND_MAX; }
float uniform_float(float min, float max) { return min + uniform_float() * (max - min); }

float normal_float()
{
    // box-mueller transform
    float u1 = uniform_float();
    float u2 = uniform_float();

    float r = sqrtf(-2.f * logf(u1));
    r *= cosf(2.f * 3.141592f * u2);
    return r;
}
float normal_float(float mean, float stddev) { return mean + normal_float() * stddev; }

}
