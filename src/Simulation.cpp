#include "Simulation.h"

#include <algorithm>
#include <fmt/core.h>

Simulation::Simulation(uint32_t number_of_queues)
    : m_last_tick(0)
{
    m_queues.resize(number_of_queues);
}

Simulation::~Simulation() { }

void Simulation::add_customer(uint32_t delay)
{
    auto it = std::min_element(m_queues.begin(), m_queues.end(),
        [&](auto const& a, auto const& b) { return a.size() < b.size(); });
    uint32_t index = std::distance(m_queues.begin(), it);
    m_queues[index].push(delay);

    CashRegisterEvent event = {
        .type = CashRegisterEvent::Type::AddCustomerToQueue,
        .target = index,
        .delay = delay,
    };
    emit(event.type, event);
}

void Simulation::tick(uint64_t ticks)
{
    if (m_last_tick == 0) {
        m_last_tick = ticks;
        return;
    }

    uint32_t delta = ticks - m_last_tick;
    // fmt::print("tick: {}, last tick: {}, delta: {}\n", ticks, m_last_tick, delta);
    m_last_tick = ticks;

    for (uint32_t i = 0; i < m_queues.size(); ++i) {
        auto& queue = m_queues[i];
        if (queue.empty())
            continue;

        int32_t curr_delay = delta;
        while (curr_delay > 0 && !queue.empty()) {
            if ((uint32_t)curr_delay < queue.front()) {
                queue.front() -= curr_delay;
                break;
            }
            // fmt::print("curr_delay: {}\n", curr_delay);
            curr_delay -= queue.front();

            queue.pop();
            CashRegisterEvent event = {
                .type = CashRegisterEvent::Type::RemoveCustomerFromQueue,
                .target = i,
                .delay = 0,
            };
            emit(event.type, event);
        }
    }
}
