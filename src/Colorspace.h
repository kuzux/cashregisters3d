#pragma once

#include <glm/glm.hpp>

namespace Colorspace
{
glm::vec3 oklab_to_linear_srgb(glm::vec3 c);
glm::vec3 linear_srgb_to_oklab(glm::vec3 c);
}
