#pragma once

#include "vendor/imgui.h"
#include <functional>

#include <glm/glm.hpp>

class ConfigUI
{
public:
    ConfigUI() { }
    ~ConfigUI() { }

    void draw();
    void on_add_customer(std::function<void(float, float)> cb) { m_add_customer_cb = cb; }

    glm::vec3 camera_pos() const { return m_camera_pos; }
    glm::mat4 view_matrix() const;

private:
    std::function<void(float, float)> m_add_customer_cb;
    glm::vec3 m_camera_pos { 0.0f, 10.0f, 5.0f };
    glm::vec3 m_camera_lookat { 0.0f, 4.0f, 0.0f };

    float m_customer_mean_time { 3.f };
    float m_customer_stddev_time { .6f };
};
