#pragma once

#include <functional>
#include <queue>
#include <string>
#include <string_view>
#include <unordered_map>

#include "Emitter.h"
#include "Error.h"

#include <SDL.h>
#include <glm/vec2.hpp>

// clang-format off
#define USE_IMGUI
#define IMGUI_HEADER(h) vendor/h
// clang-format on

class SDLEventLoop;
class SDLWindow
{
    friend class SDLEventLoop;

public:
    SDLWindow(std::string_view title, glm::ivec2 size);
    ~SDLWindow();

    void draw();
    glm::ivec2 window_size() const { return m_window_size; }
    float aspect_ratio() const { return (float)m_window_size.x / (float)m_window_size.y; }

    void set_window_size(glm::ivec2 size) { m_window_size = size; }
    void set_title(std::string_view title);
    void set_icon(std::string const& filename);
    ErrorOr<void> setup_imgui();
    SDLEventLoop& event_loop();

private:
    SDL_Window* window = nullptr;
    SDL_GLContext ctx;

    glm::ivec2 m_window_size;
    SDL_Surface* m_window_icon { nullptr };
    SDLEventLoop* m_event_loop { nullptr };
    bool m_vsynced { false };
    bool m_using_imgui { false };
};

class Timeout
{
public:
    enum class Result {
        Continue,
        Stop,
    };
    using Callback = std::function<Result(uint64_t)>;

    Timeout(uint64_t interval, Callback callback);
    ~Timeout();

    bool should_call(uint64_t ticks) const;
    Result call(uint64_t ticks);

private:
    uint64_t m_interval;
    uint64_t m_last_call;
    Callback m_callback;
};

class SDLEventLoop : public Emitter<SDL_EventType, SDL_Event>
{
    friend class SDLWindow;

public:
    // First argument is the current tick count, second is the delta ticks since last call
    void run(std::function<void(uint64_t, uint64_t)> body);
    // Only the current tick count is passed
    void run(std::function<void(uint64_t)> body);

    void in(uint64_t interval, Timeout::Callback callback);
    void stop();

private:
    SDLEventLoop(SDLWindow& window);

    bool m_running { false };
    std::unordered_map<SDL_EventType, std::function<void(SDL_Event)>> m_event_cbs;
    std::queue<Timeout> m_timeouts;

    SDLWindow& m_window;
};
