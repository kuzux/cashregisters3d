#pragma once

class Scene
{
public:
    virtual void draw(float secs) = 0;
};